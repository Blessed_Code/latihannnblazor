using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Latihannn.Entities.Entities.PostGre;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace Latihannn
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            //services.AddControllers(); // Bisa pake Web API

            // Untuk blazor
            services.AddServerSideBlazor();

            // untuk postgre
            services.AddDbContextPool<PostGreDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostGreConnString"), strategy =>
                {
                    strategy.EnableRetryOnFailure();
                }
                );
            });

            //services.AddScoped<IValidator<EmployeeCreateForm>, EmployeeCreateFormValidator>();

            // using fluent validation
            services.AddValidatorsFromAssemblyContaining<Program>();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                //endpoints.MapDefaultControllerRoute(); // Bisa pake Web API
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
